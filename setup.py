from setuptools import setup, find_packages

setup(
   name='salto',
   version='1.2.0',
   description='Salto library',
   packages=find_packages(),
   include_package_data=True,
   install_requires=['translitcodec'],
)
