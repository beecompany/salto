from datetime import datetime
from typing import List, Optional

from salto.message import Message
from salto.support.card_details import CardDetails


"""
mc_message = ModifyCheckInData(
    room="101", new_room="102", new_expiry_time=datetime.now() + timedelta(days=2)
)
"""


class ModifyCheckInData(Message):
    COMMAND_NAME: str = "MC"

    def __init__(
        self,
        room: str,
        new_room: Optional[str] = None,
        new_expiry_date: Optional[datetime] = None,
    ) -> None:
        fields: List[bytes] = [b""] * 4

        fields[0] = self.encode_str(self.COMMAND_NAME)
        fields[1] = self.encode_str(room)

        if new_room:
            fields[2] = self.encode_str(new_room)
        if new_expiry_date:
            fields[3] = self.encode_str(
                new_expiry_date.strftime(CardDetails.DATETIME_FORMAT)
            )

        super().__init__(fields)
